from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/hello')
def hello_world():
    result = 'hello'
    if 'name' in request.args:
        result = result + " " + request.args['name']
    else:
        result = result + " " 'Stranger'
    return result


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
    #app.run()
